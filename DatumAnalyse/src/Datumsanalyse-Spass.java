import java.applet.Applet;
import java.awt.Button;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.Random;

/**
 * Kreis Praktika
 *
 * @Zanchi Fabio @19.10.2018
 */
public class Datumsanalyse extends Applet implements ActionListener {

  private Label aufforderung = null;
  private TextField eingabeFeld = null;
  private Button druckKnopf = null;
  private String eingegebenesDatum = "";
  private String tag, monat, jahr;
  private int pos1, pos2;
  private int d1, d, m, y1, y, c;
  private String output = "";

  @Override
  public void init()

  {
    aufforderung = new Label("Zu analysierendes Datum :");
    this.add(aufforderung);
    eingabeFeld = new TextField("00.00.0000", 20);
    this.add(eingabeFeld);
    druckKnopf = new Button("Analysieren");
    this.add(druckKnopf);
    druckKnopf.addActionListener(this);
    druckKnopf.addMouseListener(new MouseListener() {

      @Override
      public void mouseReleased(MouseEvent e) {

      }

      @Override
      public void mousePressed(MouseEvent e) {

      }

      @Override
      public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub

      }

      @Override
      public void mouseEntered(MouseEvent e) {
        druckKnopf.setLocation(new Random().nextInt(getWidth() - 10), new Random().nextInt(getHeight() - 10));
      }

      @Override
      public void mouseClicked(MouseEvent e) {
        // TODO Auto-generated method stub

      }

    });
  }

  @Override
  public void actionPerformed(ActionEvent ereignis) {
    eingegebenesDatum = eingabeFeld.getText();
    eingabeFeld.setText("00.00.000");
    this.repaint();

    // Parse the date-string into a localized date (our timezone) using a date
    // pattern.(
    // https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html )
    final LocalDate date = LocalDate.parse(eingegebenesDatum, DateTimeFormatter.ofPattern("d.M.y"));

    // Extract the localized weekday name
    final String dayOfWeek = date.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.GERMAN);

    // Create a year dependent Month object which is capable of leap-year handling.
    final YearMonth month = YearMonth.of(date.getYear(), date.getMonth());

    // Extract the localized month name
    final String monthName = month.getMonth().getDisplayName(TextStyle.FULL, Locale.GERMAN);

    // Extract the length of the month in days
    final int daysOfMonth = month.lengthOfMonth();

    // Extract 'Ja' for a leap-year or 'Nein' for no leap-year.
    final String leapYearYesNo;
    if (month.isLeapYear()) {
      leapYearYesNo = "einem";
    } else {
      leapYearYesNo = "keinem";
    }

    // Combine the collected data into a string
    final StringBuilder sb = new StringBuilder();
    sb.append("Eingabe: ");
    sb.append(eingegebenesDatum);
    sb.append(" - Dieser Tag ist ein ");
    sb.append(dayOfWeek);
    sb.append(" im Monat ");
    sb.append(monthName);
    sb.append(", welcher sich mit ");
    sb.append(daysOfMonth);
    sb.append(" Tagen in ");
    sb.append(leapYearYesNo);
    sb.append(" Schaltjahr befindet.");
    sb.append("\r\n");

    output = sb.toString();

    // pos1 = eingegebenesDatum.indexOf(".");
    // pos2 = eingegebenesDatum.indexOf(".", pos1 + 1);
    //
    // tag = eingegebenesDatum.substring(0, pos1);
    // monat = eingegebenesDatum.substring(pos1 + 1, pos2);
    // jahr = eingegebenesDatum.substring(pos2 + 1);
    //
    // m = Integer.parseInt(monat);
    // if (m < 13) {
    // m = m;
    // } else {
    // m--;
    // }
    //
    // y1 = Integer.parseInt(jahr);
    // if (m < 13) {
    // y = y1 % 100;
    // } else {
    // y = (y1 % 100) - 1;
    // }
    //
    // c = y1 / 100;
    //
    // d1 = Integer.parseInt(tag);
    // d = (d1 + (26 * (m + 1) / 10) + y1 + (y1 / 4) + (c / 4) + 5 * c) % 7;
    //
    // // if(y1/4
  }

  @Override
  public void paint(Graphics g) {

    g.drawString(output, 10, 50);

    // g.drawString("Radius r : " + eingegebenerText + " mm", 10, 80);
    //
    // g.drawString("Umfang U : " + U + " mm^2", 10, 100);
    //
    // g.drawString("OberflÃ¤che O : " + O + " mm^2", 10, 120);
    //
    // g.drawString("Volumen V : " + V + " mm^2", 10, 140);
  }

}
