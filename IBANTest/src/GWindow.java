import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;

/**
<B>GWindow</B> implements a Java window as a subclass of javax.swing.JFrame.<P>

This window will work properly if opened from within a JAR file that is
executed from the DOS command line because it provides a method that
allows a thread to wait until the window is closed. (Without this
feature, the window is only open briefly, and then the main thread dies
and takes the JVM with it and the whole thing disappears.)<P>

Any subclass of java.awt.Component can be specified as the content of
this window. The content can be either AWT or Swing based. It is
advisable to avoid mixing Swing and AWT components in the content.<P>

You can draw in GWindow's content with graphics primitives or construct
a GUI in it (either AWT or Swing).  And since it's not an applet, you
can run it in BlueJ and still use the symbolic debugger - something you
can't do with an applet.<P>

<I>Known uses:</I> I have used this class successfully as a way to let
my students run and test code that uses Java graphics primitives.  It is
also a way to test AWT and Swing user interfaces without incurring the
debugging limitations caused by using an applet.<P>

<I>Hints for use:</I> See GWindowDemo for an example of how to use GWindow.

@version 1.1
@author Michael Trigoboff
@author Davin McCall
*/

public class GWindow
  extends JFrame
{
  private boolean     closed = false;

  /**
   creates and opens a javax.swing.JFrame based window.  The content argument
   is what will be displayed in the window.  Content can be either AWT or Swing based.
   It is advisable to avoid mixing Swing and AWT components in the content.
   */
  public GWindow (String title, int width, int height, Component content)
  {
    super(title);

    addWindowListener(new WindowAdapter()
    {
      public void windowClosed (WindowEvent e)
      {
        synchronized (GWindow.this) {
          closed = true;
          GWindow.this.notify();
          }
      }
    });
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    getContentPane().add(content, BorderLayout.CENTER);
      // default content pane layout manager is BorderLayout
    setSize(width, height);
    setVisible(true);
  }

  /**
   allows a thread to wait for this window to close
   */
  public synchronized void waitForClose ()
  {
    while (! closed)
      try { wait(); } catch (InterruptedException e) { }
  }
}
