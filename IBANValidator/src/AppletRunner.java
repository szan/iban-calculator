/**
 Demonstrates running an applet in an AppletWindow.
 */
public class AppletRunner
{
    /**
     * convenient way to call main without having to make a stop to provide arguments
     */
    public static void execute ()
    {
        main(new String[0]);
    }
        /**
     * the usual static main method
     */
    public static void main (String[] args)
    {
        AppletWindow aw1 = new AppletWindow("Datumsanalyse", 800, 600, new IBANValidierung());
    }
}
