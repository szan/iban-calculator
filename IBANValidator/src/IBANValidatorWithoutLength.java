import java.math.BigInteger;

public class IBANValidatorWithoutLength {

	private static final String IBAN = "CH03 0685 0016 6769 8750 1";

	private boolean verifyIban(String iban) {
		// Replace all whitespaces and write all characters in uppercase.
		iban = iban.replaceAll("\\s", "").toUpperCase();

		// Check if IBAN String has at least 4 characters.
		if (iban.length() < 4) {
			return false;
		}

		// Check the content of the IBAN String to only contain uppercase characters and numbers.
		if (!iban.matches("[0-9A-Z]+")) {
			return false;
		}

		// Extract the country shortcut from the IBAN String.
		String country = iban.substring(0, 2);

		// Prepare the calcSum String containing the digits of the IBAN String without country and checksum.
		String calcSumString = iban.substring(4);

		// Append the numeric value of the Country code's first letter.
		calcSumString += Character.getNumericValue(country.charAt(0));
		// Append the numeric value of the Country code's second letter.
		calcSumString += Character.getNumericValue(country.charAt(1));
		
		// Append the checksum.
		calcSumString += iban.substring(2, 4);
		
		// Create a calcSum bigint (big int due to the count of digitcs not fitting in any long, etc.
		BigInteger calcSum = new BigInteger(calcSumString);

		// Get the modulo of our calcSum and 97.
		BigInteger modulo = calcSum.mod(BigInteger.valueOf(97));
		
		// Return if the modulo is equal to 1. (true/false).
		return modulo.intValue() == 1;
	}

	public static void main(String[] args) {
		boolean valid = new IBANValidatorWithoutLength().verifyIban(IBAN);
		System.out.println(IBAN + " is " + (valid ? "valid" : "not valid"));
	}

}