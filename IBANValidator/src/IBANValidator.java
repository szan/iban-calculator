import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

public class IBANValidator {

	private static final String IBAN = "CH03 0685 0016 6769 8750 1";

	private static final Map<String, Integer> COUNTRY_DEFINITION = Collections.unmodifiableMap(Arrays
			.asList("AL28", "AD24", "AT20", "AZ28", "BE16", "BH22", "BA20", "BR29", "BG22", "HR21", "CY28", "CZ24",
					"DK18", "DO28", "EE20", "FO18", "FI18", "FR27", "GE22", "DE22", "GI23", "GL18", "GT28", "HU28",
					"IS26", "IE22", "IL23", "IT27", "KZ20", "KW30", "LV21", "LB28", "LI21", "LT20", "LU20", "MK19",
					"MT31", "MR27", "MU30", "MC27", "MD24", "ME22", "NL18", "NO15", "PK24", "PS29", "PL28", "PT25",
					"RO24", "SM27", "SA24", "RS22", "SK24", "SI19", "ES24", "SE24", "CH21", "TN24", "TR26", "AE23",
					"GB22", "VG24", "GR27", "CR21")
			.stream()
			.collect(Collectors.toMap(entry -> entry.substring(0, 2), entry -> Integer.parseInt(entry.substring(2)))));

	private boolean verifyIban(String iban) {
		// Replace all whitespaces and write all characters in uppercase.
		iban = iban.replaceAll("\\s", "").toUpperCase();

		// Check if IBAN String has at least 4 characters.
		if (iban.length() < 4) {
			return false;
		}

		// Check the content of the IBAN String to only contain uppercase characters and numbers.
		if (!iban.matches("[0-9A-Z]+")) {
			return false;
		}

		// Extract the country shortcut from the IBAN String.
		String country = iban.substring(0, 2);

		// Check if the country defininition mapping contains the given country.
		if (!COUNTRY_DEFINITION.containsKey(country)) {
			return false;
		}

		// Load the length of IBANS for the given country.
		Integer ibanLengthForCountry = COUNTRY_DEFINITION.get(country);

		// Check if the given IBAN String has the same length as the expectation for the country.
		if (ibanLengthForCountry != iban.length()) {
			return false;
		}

		// Prepare the calcSum String containing the digits of the IBAN String without country and checksum.
		String calcSumString = iban.substring(4);

		// Append the numeric value of the Country code's first letter.
		calcSumString += Character.getNumericValue(country.charAt(0));
		// Append the numeric value of the Country code's second letter.
		calcSumString += Character.getNumericValue(country.charAt(1));
		
		// Append the checksum.
		calcSumString += iban.substring(2, 4);
		
		// Create a calcSum bigint (big int due to the count of digitcs not fitting in any long, etc.
		BigInteger calcSum = new BigInteger(calcSumString);

		// Get the modulo of our calcSum and 97.
		BigInteger modulo = calcSum.mod(BigInteger.valueOf(97));
		
		// Return if the modulo is equal to 1. (true/false).
		return modulo.intValue() == 1;
	}

	public static void main(String[] args) {
		boolean valid = new IBANValidator().verifyIban(IBAN);
		System.out.println(IBAN + " is " + (valid ? "valid" : "not valid"));
	}

}